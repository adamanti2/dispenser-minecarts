from nbtlib import Byte, Float
from bolt_expressions import Data
from ./utils import scores, storage, damageModifier, MAIN_SLOTS

function ./dcart_activate:
    tag @s add dcart_activating

    data modify storage dispenserminecarts:_ CurrentItems set value []
    for slot in MAIN_SLOTS:
        data modify storage dispenserminecarts:_ CurrentItems append from entity @s Items[{Slot:Byte(slot)}]
    function ./_r_remove_filler
    function ./_r_remove_filler:
        store success score .success scores.var run data remove storage dispenserminecarts:_ CurrentItems[{components:{"minecraft:custom_data":{dcart_filler:1b}}}]
        if score .success scores.var matches 1 run function ./_r_remove_filler
    
    store result score .length scores.var run data get storage dispenserminecarts:_ CurrentItems
    if score .length scores.var matches 0:
        playsound minecraft:block.dispenser.fail neutral @a ~ ~ ~ 1 1.2

    unless score .length scores.var matches 0:
        # playsound minecraft:block.dispenser.dispense neutral @a
        # playsound minecraft:block.dispenser.launch neutral @a ~ ~ ~ 1 1.2

        scores.var['.prng'] = 1
        if score .length scores.var matches 2..:
            function ./prng
            scores.var['.prng'] %= scores.var['.length']
        
        for i in range(9):
            if score .prng scores.var matches i:
                storage.CurrentItem = storage.CurrentItems[i - 1]
        
        scores.var['.count'] = storage.CurrentItem.count
        storage.CurrentItem.count = Byte(1)
        scores.var[".slot"] = storage.CurrentItem.Slot
        for i in MAIN_SLOTS:
            if score .slot scores.var matches i:
                item replace entity @e[type=armor_stand,tag=dcart_helper] weapon from entity @s f"container.{i}" ./set_to_1
        scores.var[".reduce"] = 1
        scores.var[".drop"] = 1
        scores.var[".click"] = 1
        scores.var[".launch"] = 0
        scores.var[".fail"] = 0
        scores.var[".smoke"] = 1

        scores.var[".dropper"] = 0
        if entity @s[tag=dcart_dropper]:
            scores.var[".dropper"] = 1
        
        as @e[type=armor_stand,tag=dcart_helper]:
            positioned as @s run tp @s ~ ~ ~ ~ ~

            if score .dropper scores.var matches 1 positioned ^ ^ ^1 if block ~ ~ ~ #./dispense/droppable_containers run function ./dispense/container_insert
            
            unless score .dropper scores.var matches 1:                
                for func in (
                    "equippable", # heads never drop, armor does
                    "armor_stand", # never fails
                    "launchables", # never fails
                    "boats", # i'll change it to never drop
                    "fire_charge", # never fails
                    "firework", # never fails
                    "flint_and_steel", # never drops
                    "empty_bucket", # i'll change it to never drop
                    "filled_buckets", # i'll change it to never drop
                    "minecarts", # i'll change it to never drop
                    "shulker_boxes", # never drops
                    "tnt", # never fails
                    "shears", # never drops
                    "glass_bottle", # i'll change it to never drop
                    "animal_equippable",
                    "glowstone", # fails if facing full anchor
                    "honeycomb", # i'll change it to never drop
                    "water_bottle", # i'll change it to never drop
                    "wind_charge", # never fails
                    # "bone_meal", # never drops; impossible to implement
                    # "spawn_eggs", # never fails; but also might be impossible to implement
                ):
                    name = f"dispenserminecarts:dispense/{func}"
                    if predicate name run function name
            
            # "if predicate placeblocks:blocks" is dependent on dispensers-place-blocks
            if score .blockplacing scores.config matches 1 if score .drop scores.var matches 1 run function ./try_place_block
            function ./try_place_block:
                if predicate placeblocks:blocks positioned ^ ^ ^1 run function ./dispense/place_block
            if score .drop scores.var matches 1 run function ./dispense/generic_drop

        if score .click scores.var matches 1 run playsound minecraft:block.dispenser.dispense neutral @a
        if score .launch scores.var matches 1 run playsound minecraft:block.dispenser.launch neutral @a ~ ~ ~ 1 1.2
        if score .fail scores.var matches 1 run playsound minecraft:block.dispenser.fail neutral @a ~ ~ ~ 1 1.2
        if score .smoke scores.var matches 1 run particle minecraft:smoke ^ ^ ^1 0 0 0 0.02 7 normal
        if score .reduce scores.var matches 1:
            for i in MAIN_SLOTS:
                if score .slot scores.var matches i:
                    item modify entity @s f"container.{i}" dispenserminecarts:reduce
        if score .reduce scores.var matches 2:
            for i in MAIN_SLOTS:
                if score .slot scores.var matches i:
                    item modify entity @s f"container.{i}" damageModifier("flint_and_steel", 64, storage.CurrentItem.components["minecraft:enchantments"].levels["minecraft:unbreaking"])
        if score .reduce scores.var matches 3:
            for i in MAIN_SLOTS:
                if score .slot scores.var matches i:
                    item modify entity @s f"container.{i}" damageModifier("shears", 238, storage.CurrentItem.components["minecraft:enchantments"].levels["minecraft:unbreaking"])
        if score .reduce scores.var matches 4:
            for i in MAIN_SLOTS:
                if score .slot scores.var matches i:
                    item replace entity @s f"container.{i}" with glass_bottle
        if score .reduce scores.var matches 5:
            for i in MAIN_SLOTS:
                if score .slot scores.var matches i:
                    item replace entity @s f"container.{i}" from entity @e[type=armor_stand,tag=dcart_helper,limit=1] weapon.offhand
        if score .reduce scores.var matches 6:
            for i in MAIN_SLOTS:
                if score .slot scores.var matches i:
                    item replace entity @s f"container.{i}" with bucket
    
    tag @s remove dcart_activating


item_modifier ./set_to_1 {
    "function": "minecraft:set_count",
    "count": 1
}

item_modifier ./reduce {
    "function": "minecraft:set_count",
    "count": -1,
    "add": true
}
