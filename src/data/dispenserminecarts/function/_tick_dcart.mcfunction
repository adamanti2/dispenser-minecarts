from nbtlib import Byte, Float
from ./utils import scores, tickers, angle_in_range, angle_in_range_ie, angle_in_range_ei, minecartDirections, minecartDirectionsReversed, railAngles, railAnglesDiagonal

function ./tick_dcart:
    as @e[type=chest_minecart,tag=dcart] at @s:

        # minecarts on diagonal rails snap their rotation for some reason so i'm updating it 1 tick later
        if entity @s[tag=dcart_new_diagonal]:
            tag @s remove dcart_new_diagonal
            for dir, angle in railAnglesDiagonal.items():
                anglef = Float(angle)
                if block ~ ~ ~ #minecraft:rails[shape=dir]:
                    data modify entity @s Rotation[0] set value anglef

        # minecarts have extremely wacky client-side display when it comes to rotation so this slightly remedies that
        # (although there's no complete helping the fact that minecarts weren't designed for their contents to face a specific direction)
        if score 2 tickers matches 1:
            scores.var['.checked'] = 0
            def thingy(dirdict):
                scores.var['.checked'] = 1
                for dir in minecartDirections.keys():
                    minecartdir = dirdict[dir]
                    tag1 = f"dcart_{dir}"
                    tag2 = f"dcart_facing_{minecartdir}"
                    if entity @s[tag=tag1,tag=!tag2]:
                        tag @s remove dcart_facing_up
                        tag @s remove dcart_facing_north
                        tag @s remove dcart_facing_east
                        tag @s remove dcart_facing_south
                        tag @s remove dcart_facing_west
                        tag @s add tag2
                        data modify entity @s DisplayState.Properties.facing set value minecartdir
            unless block ~ ~ ~ #minecraft:rails:
                thingy(minecartDirections)
            if score .checked scores.var matches 0 if entity @s[y_rotation=91..180]:
                thingy(minecartDirections)
            if score .checked scores.var matches 0 if entity @s[y_rotation=226..315]:
                thingy(minecartDirections)
            if score .checked scores.var matches 0:
                thingy(minecartDirectionsReversed)
        
        # hopper check
        if score .fillerdyes scores.config matches 1 if score 2 tickers matches 0:
            scores.var['.hopper'] = 0
            if predicate ./near_hopper:
                scores.var['.hopper'] = 1
            unless score .hopper scores.var matches 1 if entity @s[tag=dcart_near_hopper_minecart] align xyz positioned ~-1 ~-1 ~-1 if entity @e[type=minecraft:hopper_minecart,dx=2,dy=1,dz=2]:
                    scores.var['.hopper'] = 1
            unless entity @s[tag=dcart_over_hopper] if score .hopper scores.var matches 1:
                tag @s add dcart_over_hopper
                function ./refresh_dcart_filler
            if entity @s[tag=dcart_over_hopper] if score .hopper scores.var matches 0:
                tag @s remove dcart_over_hopper

        # hopper minecart check reset
        if score .fillerdyes scores.config matches 1 if score 20 tickers matches 0 if entity @s[tag=dcart_near_hopper_minecart]:
            unless entity @e[type=hopper_minecart,distance=..10] run tag @s remove dcart_near_hopper_minecart

        # actual minecart powering logic
        if entity @s[tag=dcart_powered] unless block ~ ~ ~ minecraft:activator_rail[powered=true]:
            tag @s remove dcart_powered
        unless entity @s[tag=dcart_powered] if block ~ ~ ~ minecraft:activator_rail[powered=true] positioned ~ ~0.75 ~:
            tag @s add dcart_powered
            if entity @s[tag=dcart_front] run function ./dcart_activate
            if entity @s[tag=dcart_right] rotated ~90 0 run function ./dcart_activate
            if entity @s[tag=dcart_back] rotated ~180 0 run function ./dcart_activate
            if entity @s[tag=dcart_left] rotated ~-90 0 run function ./dcart_activate
            if entity @s[tag=dcart_up] rotated ~ -90 run function ./dcart_activate


advancement ./on_minecart_filler_changed {
    "criteria": { "requirement": {
            "trigger": "minecraft:inventory_changed",
            "conditions": { "items": [ { "items": [
                            "minecraft:gray_dye"
                        ],
                        "components": {
                            "minecraft:custom_data": "{dcart_filler:1b}"
                        }
            } ] } } },
    "rewards": {
        "function": "dispenserminecarts:on_minecart_filler_changed"
    },
    "sends_telemetry_event": false
}

function ./on_minecart_filler_changed:
    advancement revoke @s only ./on_minecart_filler_changed
    clear @s gray_dye[custom_data={dcart_filler:1b}]
    as @e[type=chest_minecart,tag=dcart,distance=..11] run function ./refresh_dcart_filler


advancement ./on_dcart_interact {
    "criteria": { "requirement": {
            "trigger": "minecraft:player_interacted_with_entity",
            "conditions": {
                "player": [ {
                        "condition": "minecraft:inverted",
                        "term": {
                            "condition": "minecraft:entity_properties",
                            "entity": "this",
                            "predicate": { "type_specific": {
                                    "type": "player",
                                    "gamemode": [ "spectator" ]
                            } }
                        }
                } ],
                "entity": {
                    "type": "minecraft:chest_minecart",
                    "team": "dcart"
                }
            }
    } },
    "rewards": {
        "function": "dispenserminecarts:on_dcart_interact"
    },
    "sends_telemetry_event": false
}

# When player opens chest minecart
function ./on_dcart_interact:
    advancement revoke @s only ./on_dcart_interact
    clear @s gray_dye[custom_data={dcart_filler:1b}]

    tag @s add interacting_dcart
    # Placeholder values that tell ./tick_20 to avoid checking the player's rotation
    # The actual values are scheduled to set in 10t to account for the fact that a player might still be slightly turning when opening the minecart
    scores.playerR0['@s'] = 0
    scores.playerR1['@s'] = -1000
    schedule function ./update_interacting_rotations 10t append

    # Raycast to find all minecarts within the players view
    scores.var['.found'] = 0
    scores.var['.steps'] = 6
    anchored eyes positioned ^ ^ ^ run function ./_r_on_dcart_interact
    function ./_r_on_dcart_interact:
        as @e[type=chest_minecart,tag=dcart,tag=!dcart_interacting,distance=..1] run function ./_dcart_start_interact
        scores.var['.steps'] -= 1
        unless score .steps scores.var matches ..0 positioned ^ ^ ^1 run function ./_r_on_dcart_interact
    
    # If no minecarts found in the raycast just match all of them in a 6-block radius
    if score .found scores.var matches 0 as @e[type=chest_minecart,tag=dcart,tag=!dcart_interacting,distance=..6] run function ./_dcart_start_interact

    function ./_dcart_start_interact:
        scores.var['.found'] = 1
        tag @s add dcart_interacting
        function ./refresh_dcart_filler

# Only scheduled in ./on_dcart_interact
function ./update_interacting_rotations:
    as @a[tag=interacting_dcart]:
        store result score @s scores.playerR0 run data get entity @s Rotation[0] 10
        store result score @s scores.playerR1 run data get entity @s Rotation[1] 10

function ./refresh_dcart_filler:
    # Remove filler items from the main slots
    data modify storage dispenserminecarts:_ CurrentItems set from entity @s Items
    if score .fillerdyes scores.config matches 1:
        scores.var['.removed'] = 0
        mainSlots = (0, 1, 2, 9, 10, 11, 18, 19, 20)
        for slot in mainSlots:
            store success score .success scores.var run data remove storage dispenserminecarts:_ CurrentItems[{Slot:Byte(slot),components:{"minecraft:custom_data":{dcart_filler:1b}}}]
            scores.var['.removed'] += scores.var['.success']
        if score .removed scores.var matches 1.. run data modify entity @s Items set from storage dispenserminecarts:_ CurrentItems

    # Drop any rogue items in the filler slots
    for slot in mainSlots:
        data remove storage dispenserminecarts:_ CurrentItems[{Slot:Byte(slot)}]
    function ./_r_remove_filler
    function ./_r_remove_filler:
        store success score .success scores.var run data remove storage dispenserminecarts:_ CurrentItems[{components:{"minecraft:custom_data":{dcart_filler:1b}}}]
        if score .success scores.var matches 1 run function ./_r_remove_filler
    store result score .items scores.var run data get storage dispenserminecarts:_ CurrentItems
    if score .items scores.var matches 1.. at @s:
        function ./_r_drop_rogue_item
        function ./_r_drop_rogue_item:
            loot spawn ~ ~ ~ loot ./dcart_filler
            as @e[type=item,tag=!dcart_checked,distance=0]:
                tag @s add dcart_checked
                data modify entity @s Item set from storage dispenserminecarts:_ CurrentItems[0]
            data remove storage dispenserminecarts:_ CurrentItems[0]
            scores.var['.items'] -= 1
            if score .items scores.var matches 1.. run function ./_r_drop_rogue_item

    # Replace all the filler items in the filler slots
    if score .fillerdyes scores.config matches 1:
        scores.var['.fillers'] = 1
        if entity @s[tag=dcart_over_hopper]:
            scores.var['.fillers'] = 0
        if score .fillers scores.var matches 0:
            for n in tuple(range(3, 9)) + tuple(range(12, 18)) + tuple(range(21, 27)):
                slot = f"container.{n}"
                item replace entity @s slot with air
        if score .fillers scores.var matches 1:
            for n in tuple(range(3, 9)) + tuple(range(12, 18)) + tuple(range(21, 27)):
                slot = f"container.{n}"
                item replace entity @s slot with gray_dye[custom_name='""',custom_data={dcart_filler:1b}]

loot_table ./dcart_filler {
  "pools": [
    {
      "rolls": 1,
      "entries": [
        {
          "type": "minecraft:item",
          "name": "minecraft:gray_dye",
          "functions": [
            {
              "function": "minecraft:set_name",
              "name": ""
            },
            {
              "function": "minecraft:set_custom_data",
              "tag": "{dcart_filler:1b}"
            }
          ]
        }
      ]
    }
  ]
}
