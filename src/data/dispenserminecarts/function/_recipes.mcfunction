import json

for n, i in enumerate(("dispenser", "dropper")):
    modelData = 5 + n
    name = {"translate":"item.dispenserminecarts." + i + "_minecart","fallback":"Minecart with " + i.capitalize(),"italic":false,"color":"white"}

    recipeName  = f"dispenserminecarts:{i}_minecart"
    recipe recipeName {
      "type": "minecraft:crafting_shapeless",
      "ingredients": [
        {
          "item": f"minecraft:{i}"
        },
        {
          "item": "minecraft:minecart"
        }
      ],
      "result": {
        "id": "minecraft:furnace_minecart",
        "components": {
          "minecraft:enchantment_glint_override": true,
          "minecraft:custom_model_data": modelData,
          "minecraft:custom_name": json.dumps(name)
        }
      }
    }

    lootTableName = f"dispenserminecarts:items/{i}_minecart"
    loot_table lootTableName  {
        "type": "minecraft:command",
        "pools": [
            {
                "rolls": 1,
                "entries": [
                    {
                        "type": "minecraft:item",
                        "name": "minecraft:furnace_minecart",
                        "functions": [
                            {
                                "function": "minecraft:set_components",
                                "components": {
                                    "minecraft:enchantment_glint_override": true
                                },
                                "conditions": [
                                    # {
                                    #     "condition": "minecraft:value_check",
                                    #     "value": {
                                    #         "type": "minecraft:score",
                                    #         "target": {
                                    #             "type": "minecraft:fixed",
                                    #             "name": ".cartglint"
                                    #         },
                                    #         "score": "dcart.config"
                                    #     },
                                    #     "range": 1
                                    # }
                                ]
                            },
                            {
                                "function": "minecraft:set_custom_model_data",
                                "value": modelData
                            },
                            {
                                "function": "minecraft:set_name",
                                "name": name
                            }
                        ]
                    }
                ]
            }
        ]
    }

