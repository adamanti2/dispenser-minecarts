from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, randomizedMotion

predicate ./firework {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/firework"
            }
        }
    }
}

item_tag ./firework:
    values:
        - "firework_rocket"


scores.var['.drop'] = 0
scores.var['.click'] = 1
scores.var['.launch'] = 1

randomizedMotion(0.5, -1, 1, -1, 1)

scores.var['.flight'] = storage.CurrentItem.components['minecraft:fireworks'].flight_duration
scores.var['.flight'] += 1
scores.var['.flight'] *= 10
function ../prng
scores.var['.prng'] %= 5
scores.var['.flight'] += scores.var['.prng']
function ../prng
scores.var['.prng'] %= 6
scores.var['.flight'] += scores.var['.prng']

summon firework_rocket ^ ^ ^1 {Tags:[dcart_new],ShotAtAngle:1b}
as @e[type=firework_rocket,tag=dcart_new]:
    tag @s remove dcart_new
    Data.entity('@s')['Motion'] = storage['Motion']
    Data.entity('@s')['FireworksItem'] = storage['CurrentItem']
    Data.entity('@s')['LifeTime'] = scores.var['.flight']
