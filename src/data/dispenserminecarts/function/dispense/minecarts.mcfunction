from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate

predicate ./minecarts {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/minecarts"
            }
        }
    }
}

MINECARTS = ("minecart", "hopper_minecart", "chest_minecart", "furnace_minecart", "tnt_minecart")

item_tag ./minecarts {
    "values": MINECARTS
}


scores.var['.drop'] = 0
positioned ^ ^ ^1 if block ~ ~ ~ #rails:
    for cart in MINECARTS:
        if predicate heldPredicate(cart):
            summon cart ~ ~ ~ {Tags:[dcart_new]}
            as @e[type=cart,tag=dcart_new]:
                tag @s remove dcart_new
                Data.entity('@s').Motion = storage.Motion
                Data.entity('@s').CustomName = storage.CurrentItem.components["minecraft:custom_name"]
unless block ^ ^ ^1 #rails:
    scores.var['.reduce'] = 0
    scores.var['.click'] = 0
    scores.var['.fail'] = 1

