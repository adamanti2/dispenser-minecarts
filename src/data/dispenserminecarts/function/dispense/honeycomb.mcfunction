from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/honeycomb", "dispense/honeycomb")

item_tag ./honeycomb:
    values:
        - "honeycomb"

WAXABLE = (
    "copper_block",
    "cut_copper",
    "cut_copper_stairs",
    "cut_copper_slab",
	"chiseled_copper",
	"copper_grate",
	"copper_bulb",
	"copper_trapdoor",
	"copper_door",
    "exposed_copper",
    "exposed_cut_copper",
    "exposed_cut_copper_stairs",
    "exposed_cut_copper_slab",
	"exposed_chiseled_copper",
	"exposed_copper_grate",
	"exposed_copper_bulb",
	"exposed_copper_trapdoor",
	"exposed_copper_door",
    "weathered_copper",
    "weathered_cut_copper",
    "weathered_cut_copper_stairs",
    "weathered_cut_copper_slab",
	"weathered_chiseled_copper",
	"weathered_copper_grate",
	"weathered_copper_bulb",
	"weathered_copper_trapdoor",
	"weathered_copper_door",
    "oxidized_copper",
    "oxidized_cut_copper",
    "oxidized_cut_copper_stairs",
    "oxidized_cut_copper_slab",
	"oxidized_chiseled_copper",
	"oxidized_copper_grate",
	"oxidized_copper_bulb",
	"oxidized_copper_trapdoor",
	"oxidized_copper_door",
)

block_tag ./honeycomb/waxable {
    "values": WAXABLE
}


scores.var['.drop'] = 0
scores.var['.success'] = 0

positioned ^ ^ ^1 if block ~ ~ ~ #./honeycomb/waxable:
    scores.var['.success'] = 1
    playsound minecraft:item.honeycomb.wax_on block @a
    align xyz run particle minecraft:wax_on ~0.5 ~0.5 ~0.5 0.3 0.3 0.3 0.1 50 force
    for copperBlock in WAXABLE:
        if block ~ ~ ~ copperBlock:
            if "_slab" in copperBlock:
                for waterloggedState in ("false", "true"):
                    for typeState in ("bottom", "top", "double"):
                        if block ~ ~ ~ f"{copperBlock}[waterlogged={waterloggedState},type={typeState}]" run setblock ~ ~ ~ f"waxed_{copperBlock}[waterlogged={waterloggedState},type={typeState}]"
            elif "_stairs" in copperBlock:
                for waterloggedState in ("false", "true"):
                    for facingState in ("north", "east", "south", "west"):
                        for halfState in ("bottom", "top"):
                            for shapeState in ("straight", "inner_left", "inner_right", "outer_left", "outer_right"):
                                if block ~ ~ ~ f"{copperBlock}[waterlogged={waterloggedState},facing={facingState},half={halfState},shape={shapeState}]" run setblock ~ ~ ~ f"waxed_{copperBlock}[waterlogged={waterloggedState},facing={facingState},half={halfState},shape={shapeState}]"
            elif "_trapdoor" in copperBlock:
                for waterloggedState in ("false", "true"):
                    for facingState in ("north", "east", "south", "west"):
                        for halfState in ("bottom", "top"):
                            for openState in ("true", "false"):
                                for poweredState in ("true", "false"):
                                    if block ~ ~ ~ f"{copperBlock}[waterlogged={waterloggedState},facing={facingState},half={halfState},open={openState},powered={poweredState}]" run setblock ~ ~ ~ f"waxed_{copperBlock}[waterlogged={waterloggedState},facing={facingState},half={halfState},open={openState},powered={poweredState}]"
            elif "_door" in copperBlock:
                for hingeState in ("left", "right"):
                    for facingState in ("north", "east", "south", "west"):
                        for openState in ("true", "false"):
                            for poweredState in ("true", "false"):
                                if block ~ ~ ~ f"{copperBlock}[half=lower,hinge={hingeState},facing={facingState},open={openState},powered={poweredState}]":
                                    fill ~ ~ ~ ~ ~1 ~ air replace
                                    setblock ~ ~ ~ f"waxed_{copperBlock}[half=lower,hinge={hingeState},facing={facingState},open={openState},powered={poweredState}]"
                                    setblock ~ ~1 ~ f"waxed_{copperBlock}[half=upper,hinge={hingeState},facing={facingState},open={openState},powered={poweredState}]"
                                if block ~ ~ ~ f"{copperBlock}[half=upper,hinge={hingeState},facing={facingState},open={openState},powered={poweredState}]":
                                    fill ~ ~ ~ ~ ~-1 ~ air replace
                                    setblock ~ ~ ~ f"waxed_{copperBlock}[half=upper,hinge={hingeState},facing={facingState},open={openState},powered={poweredState}]"
                                    setblock ~ ~-1 ~ f"waxed_{copperBlock}[half=lower,hinge={hingeState},facing={facingState},open={openState},powered={poweredState}]"
            elif "_bulb" in copperBlock:
                for litState in ("false", "true"):
                    for poweredState in ("false", "true"):
                        if block ~ ~ ~ f"{copperBlock}[lit={litState},powered={poweredState}]" run setblock ~ ~ ~ f"waxed_{copperBlock}[lit={litState},powered={poweredState}]"
            else:
                setblock ~ ~ ~ f"waxed_{copperBlock}"

unless score .success scores.var matches 1:
    scores.var['.reduce'] = 0
    scores.var['.click'] = 0
    scores.var['.fail'] = 1
