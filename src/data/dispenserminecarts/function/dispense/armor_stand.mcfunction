from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/armor_stand", "dispense/armor_stand")

item_tag ./armor_stand:
    values:
        - "minecraft:armor_stand"


scores.var['.drop'] = 0

positioned ^ ^ ^1 align y run summon armor_stand ~ ~ ~ {Tags:["dcart_new_armor_stand"]}
as @e[type=armor_stand,tag=dcart_new_armor_stand] positioned as @s:
    tag @s remove dcart_new_armor_stand
    tp @s ~ ~ ~ ~ ~
