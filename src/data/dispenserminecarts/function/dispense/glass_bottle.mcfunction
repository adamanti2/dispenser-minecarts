from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/glass_bottle", "dispense/glass_bottle")

item_tag ./glass_bottle:
    values:
        - "glass_bottle"

block_tag ./glass_bottle/water_source:
    values:
        - "water"

block_tag ./glass_bottle/bee_block:
    values:
        - "beehive"
        - "bee_nest"


scores.var['.success'] = 0
positioned ^ ^ ^1 if block ~ ~ ~ #./glass_bottle/water_source:
    scores.var['.success'] = 1
    scores.var['.drop'] = 1
    data modify storage dispenserminecarts:_ CurrentItem set value {"id":"minecraft:potion",count:Byte(1),components:{"minecraft:potion_contents":{"potion":"minecraft:water"}}}

unless score .success scores.var matches 1 positioned ^ ^ ^1 if block ~ ~ ~ #./glass_bottle/bee_block[honey_level=5]:
    scores.var['.success'] = 1
    scores.var['.drop'] = 1
    data modify storage dispenserminecarts:_ CurrentItem set value {"id":"minecraft:honey_bottle",count:Byte(1)}

    storage.Hive = Data.block("~ ~ ~")
    for beeblock in ("beehive", "bee_nest"):
        for direction in ("north", "east", "south", "west"):
            if block ~ ~ ~ f"{beeblock}[facing={direction}]" run setblock ~ ~ ~ f"{beeblock}[facing={direction},honey_level=0]"
    Data.block("~ ~ ~").Bees = storage.Hive.Bees
    Data.block("~ ~ ~").FlowerPos = storage.Hive.FlowerPos

unless score .success scores.var matches 1:
    scores.var['.drop'] = 0
    scores.var['.reduce'] = 0
    scores.var['.click'] = 0
    scores.var['.fail'] = 1
