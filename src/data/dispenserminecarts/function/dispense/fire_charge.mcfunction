from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, randomizedMotion

predicate ./fire_charge {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/fire_charge"
            }
        }
    }
}

item_tag ./fire_charge:
    values:
        - "fire_charge"


scores.var['.drop'] = 0
scores.var['.click'] = 0
playsound minecraft:entity.blaze.shoot neutral @a

randomizedMotion(0.1, -6, 6, -6, 6)

summon small_fireball ^ ^ ^0.5 {Tags:[dcart_new],acceleration_power:0.1d}
as @e[type=small_fireball,tag=dcart_new]:
    tag @s remove dcart_new
    Data.entity('@s')['Motion'] = storage['Motion']
