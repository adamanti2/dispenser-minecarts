from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate

predicate ./tnt {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/tnt"
            }
        }
    }
}

item_tag ./tnt:
    values:
        - "tnt"


scores.var['.drop'] = 0

positioned ~ ~-0.75 ~ run summon tnt ^ ^ ^1 {fuse:80s}
playsound minecraft:entity.tnt.primed block @a
