from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate

heldTagPredicate("dispense/animal_equippable", "dispense/animal_equippable")

item_tag ./animal_equippable:
    values:
        - "saddle"
        - "chest"
        - "#dispenserminecarts:dispense/animal_equippable/horse_armor"
        - "#dispenserminecarts:dispense/animal_equippable/carpets"

item_tag ./animal_equippable/horse_armor:
    values:
        - "leather_horse_armor"
        - "iron_horse_armor"
        - "golden_horse_armor"
        - "diamond_horse_armor"

item_tag ./animal_equippable/carpets:
    values:
        - "white_carpet"
        - "orange_carpet"
        - "magenta_carpet"
        - "light_blue_carpet"
        - "yellow_carpet"
        - "lime_carpet"
        - "pink_carpet"
        - "gray_carpet"
        - "light_gray_carpet"
        - "cyan_carpet"
        - "purple_carpet"
        - "blue_carpet"
        - "brown_carpet"
        - "green_carpet"
        - "red_carpet"
        - "black_carpet"


entity_type_tag ./animal_equippable:
    values:
        - "#dispenserminecarts:dispense/animal_equippable/saddle"
        - "#dispenserminecarts:dispense/animal_equippable/chest"
        - "#dispenserminecarts:dispense/animal_equippable/horse_armor"
        - "#dispenserminecarts:dispense/animal_equippable/carpets"

entity_type_tag ./animal_equippable/saddle:
    values:
        - "pig"
        - "horse"
        - "donkey"
        - "mule"
        - "strider"
        - "camel"

entity_type_tag ./animal_equippable/chest:
    values:
        - "llama"
        - "trader_llama"
        - "donkey"
        - "mule"

entity_type_tag ./animal_equippable/horse_armor:
    values:
        - "horse"

entity_type_tag ./animal_equippable/carpets:
    values:
        - "llama"
        - "trader_llama"

entity_type_tag ./animal_equippable/needs_to_be_tamed:
    values:
        - "horse"
        - "donkey"
        - "mule"
        - "llama"
        - "trader_llama"

entity_type_tag ./animal_equippable/saddle_nbt:
    values:
        - "pig"
        - "strider"


def itemTag(itemname):
    return f"dispenserminecarts:dispense/animal_equippable/{itemname}"
ITEMS = {
    "saddle": 0,
    "chest": 1,
    "horse_armor": 2,
    "carpets": 3
}
itemPredicates = {
    "saddle": heldPredicate("saddle"),
    "chest": heldPredicate("chest"),
    "horse_armor": heldTagPredicate("dispense/animal_equippable/horse_armor"),
    "carpets": heldTagPredicate("dispense/animal_equippable/carpets"),
}
for item, value in ITEMS.items():
    if predicate itemPredicates[item]:
        scores.var['.item'] = value

scores.var['.checkedMultiple'] = 0
positioned ^ ^ ^1 align xyz run function ./animal_equippable/_r_pick_entity
function ./animal_equippable/_r_pick_entity:
    as @e[type=#./animal_equippable,dx=0,dy=0,dz=0,limit=1,sort=random,tag=!dcart_checked]:

        unless entity @s[tag=dcart_equip] if score .item scores.var matches ITEMS["saddle"] if entity @s[type=#./animal_equippable/saddle] unless data entity @s {SaddleItem:{}} unless data entity @s {Saddle:1b} run tag @s add dcart_equip
        unless entity @s[tag=dcart_equip] if score .item scores.var matches ITEMS["chest"] if entity @s[type=#./animal_equippable/chest] unless data entity @s {ChestedHorse:1b} run tag @s add dcart_equip
        unless entity @s[tag=dcart_equip] if score .item scores.var matches ITEMS["horse_armor"] if entity @s[type=#./animal_equippable/horse_armor] unless data entity @s {ArmorItem:{}} run tag @s add dcart_equip
        unless entity @s[tag=dcart_equip] if score .item scores.var matches ITEMS["carpets"] if entity @s[type=#./animal_equippable/carpets] unless data entity @s {DecorItem:{}} run tag @s add dcart_equip
        
        if entity @s[tag=dcart_equip,type=#./animal_equippable/needs_to_be_tamed] unless data entity @s {Tame:1b} run tag @s remove dcart_equip
        
        unless entity @s[tag=dcart_equip]:
            scores.var['.checkedMultiple'] = 1
            tag @s add dcart_checked
            function ./animal_equippable/_r_pick_entity
        
        if entity @s[tag=dcart_equip] at @s:
            tag @s remove dcart_equip
            scores.var['.drop'] = 0
            
            if entity @s[type=#./animal_equippable/saddle,type=#./animal_equippable/saddle_nbt]:
                Data.entity('@s').Saddle = Byte(1)
            if entity @s[type=#./animal_equippable/saddle,type=!#./animal_equippable/saddle_nbt]:
                item replace entity @s horse.saddle from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
            if entity @s[type=#./animal_equippable/chest]:
                item replace entity @s horse.chest from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
            if entity @s[type=#./animal_equippable/horse_armor]:
                item replace entity @s armor.body from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
            if entity @s[type=#./animal_equippable/carpets]:
                item replace entity @s armor.body from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon

if score .checkedMultiple scores.var matches 1 run tag @e[type=#./animal_equippable,tag=dcart_checked] remove dcart_checked

