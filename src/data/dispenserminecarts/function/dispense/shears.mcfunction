from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate

predicate ./shears {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/shears"
            }
        }
    }
}

item_tag ./shears:
    values:
        - "shears"


entity_type_tag ./shears/shearable:
    values:
        - "sheep"
        - "mooshroom"
        - "snow_golem"
        - "bogged"

block_tag ./shears/bee_block:
    values:
        - "beehive"
        - "bee_nest"

loot_table ./shears/3_honeycomb {
    "pools": [
        {
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:item",
                    "name": "minecraft:honeycomb",
                    "functions": [
                        {
                            "function": "minecraft:set_count",
                            "count": 3
                        }
                    ]
                }
            ]
        }
    ]
}

loot_table ./shears/5_red_mushroom {
    "pools": [
        {
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:item",
                    "name": "minecraft:red_mushroom",
                    "functions": [
                        {
                            "function": "minecraft:set_count",
                            "count": 5
                        }
                    ]
                }
            ]
        }
    ]
}

loot_table ./shears/5_brown_mushroom {
    "pools": [
        {
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:item",
                    "name": "minecraft:brown_mushroom",
                    "functions": [
                        {
                            "function": "minecraft:set_count",
                            "count": 5
                        }
                    ]
                }
            ]
        }
    ]
}

loot_table ./shears/carved_pumpkin {
    "pools": [
        {
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:item",
                    "name": "minecraft:carved_pumpkin"
                }
            ]
        }
    ]
}

loot_table ./shears/mushroom {
    "pools": [
        {
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:item",
                    "name": "minecraft:brown_mushroom"
                },
                {
                    "type": "minecraft:item",
                    "name": "minecraft:red_mushroom"
                }
            ]
        }
    ]
}

COLORS = ("white", "orange", "magenta", "light_blue", "yellow", "lime", "pink", "gray", "light_gray", "cyan", "purple", "blue", "brown", "green", "red", "black")

for color in COLORS:
    loottable = f"dispenserminecarts:dispense/shears/sheep_{color}"
    loot_table loottable {
        "pools": [
            {
                "rolls": 1,
                "entries": [
                    {
                        "type": "minecraft:item",
                        "name": f"minecraft:{color}_wool",
                        "functions": [
                            {
                                "function": "minecraft:set_count",
                                "count": {
                                    "min": 1,
                                    "max": 3
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }


scores.var['.drop'] = 0
scores.var['.reduce'] = 0

scores.var['.honey'] = 0
positioned ^ ^ ^1 if block ~ ~ ~ #./shears/bee_block[honey_level=5] align xyz positioned ~0.5 ~ ~0.5:
    scores.var['.honey'] = 1
    loot spawn ~ ~ ~ loot ./shears/3_honeycomb
    storage.Hive = Data.block("~ ~ ~")
    for beeblock in ("beehive", "bee_nest"):
        for direction in ("north", "east", "south", "west"):
            if block ~ ~ ~ f"{beeblock}[facing={direction}]" run setblock ~ ~ ~ f"{beeblock}[facing={direction},honey_level=0]"
    Data.block("~ ~ ~").Bees = storage.Hive.Bees
    Data.block("~ ~ ~").FlowerPos = storage.Hive.FlowerPos

unless score .honey scores.var matches 1:
    scores.var['.checkedMultiple'] = 0
    scores.var['.success'] = 0
    positioned ^ ^ ^1 align xyz run function ./shears/_r_pick_entity
    function ./shears/_r_pick_entity:
        as @e[type=#./shears/shearable,dx=0,dy=0,dz=0,limit=1,sort=random,tag=!dcart_checked]:
            if entity @s[type=snow_golem] if data entity @s {Pumpkin:1b}:
                tag @s add dcart_shear
            if entity @s[type=bogged] if data entity @s {sheared:0b}:
                tag @s add dcart_shear
            unless entity @s[tag=dcart_shear] if entity @s[type=mooshroom]:
                scores.var['.age'] = Data.entity('@s').Age
                if score .age scores.var matches 0.. run tag @s add dcart_shear
            unless entity @s[tag=dcart_shear] if entity @s[type=sheep] if data entity @s {Sheared:0b}:
                scores.var['.age'] = Data.entity('@s').Age
                if score .age scores.var matches 0.. run tag @s add dcart_shear
            
            unless entity @s[tag=dcart_shear]:
                scores.var['.checkedMultiple'] = 1
                tag @s add dcart_checked
                function ./shears/_r_pick_entity
            
            if entity @s[tag=dcart_shear] at @s:
                tag @s remove dcart_shear
                if score .checkedMultiple scores.var matches 1 run tag @e[type=#./shears/shearable,tag=dcart_checked] remove dcart_checked
                
                scores.var['.success'] = 1
                scores.var['.reduce'] = 3
                playsound minecraft:entity.sheep.shear neutral @a

                if entity @s[type=bogged]:
                    Data.entity('@s').sheared = Byte(1)
                    for i in range(2):
                        loot spawn ~ ~ ~ loot ./shears/mushroom
                
                if entity @s[type=snow_golem]:
                    Data.entity('@s').Pumpkin = Byte(0)
                    loot spawn ~ ~ ~ loot ./shears/carved_pumpkin
                
                if entity @s[type=mooshroom]:
                    particle minecraft:explosion ~ ~ ~ 0 0 0 0 1
                    summon cow ~ ~ ~
                    if data entity @s {Type:"red"}:
                        loot spawn ~ ~ ~ loot ./shears/5_red_mushroom
                    if data entity @s {Type:"brown"}:
                        loot spawn ~ ~ ~ loot ./shears/5_brown_mushroom
                    tp @s ~ -128 ~
                    kill @s
                
                if entity @s[type=sheep]:
                    Data.entity('@s').Sheared = Byte(1)
                    for i, color in enumerate(COLORS):
                        loottable = f"dispenserminecarts:dispense/shears/sheep_{color}"
                        if data entity @s {Color:Byte(i)} run loot spawn ~ ~ ~ loot loottable

    unless score .success scores.var matches 1:
        if score .checkedMultiple scores.var matches 1 run tag @e[type=#./shears/shearable,tag=dcart_checked] remove dcart_checked
        scores.var['.click'] = 0
        scores.var['.fail'] = 1
