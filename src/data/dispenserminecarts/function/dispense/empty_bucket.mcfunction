from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/empty_bucket", "dispense/empty_bucket")

item_tag ./empty_bucket:
    values:
        - "bucket"

block_tag ./empty_bucket/fillable:
    values:
        - "water"
        - "lava"
        - "powder_snow"


scores.var['.drop'] = 0
scores.var['.reduce'] = 0

positioned ^ ^ ^1 unless block ~ ~ ~ #./empty_bucket/fillable:
    scores.var['.click'] = 0
    scores.var['.fail'] = 1

positioned ^ ^ ^1 if block ~ ~ ~ #./empty_bucket/fillable:
    if score .count scores.var matches 1:
        scores.var['.reduce'] = 5
        if block ~ ~ ~ water run item replace entity @s weapon.offhand with water_bucket
        if block ~ ~ ~ lava run item replace entity @s weapon.offhand with lava_bucket
        if block ~ ~ ~ powder_snow run item replace entity @s weapon.offhand with powder_snow_bucket
    
    if score .count scores.var matches 2..:
        scores.var['.drop'] = 1
        scores.var['.reduce'] = 1
        if block ~ ~ ~ water run data modify storage dispenserminecarts:_ CurrentItem set value {"id":"minecraft:water_bucket",count:Byte(1)}
        if block ~ ~ ~ lava run data modify storage dispenserminecarts:_ CurrentItem set value {"id":"minecraft:lava_bucket",count:Byte(1)}
        if block ~ ~ ~ powder_snow run data modify storage dispenserminecarts:_ CurrentItem set value {"id":"minecraft:powder_snow_bucket",count:Byte(1)}
    setblock ~ ~ ~ air
