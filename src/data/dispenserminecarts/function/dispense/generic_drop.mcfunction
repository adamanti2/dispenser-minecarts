from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, randomizedMotion

randomizedMotion(0.325, -20, 20, -40, 0)

summon item ^ ^ ^0.5 {Tags:[dcart_new_item,dcart_air_toggle],Item:{count:1b,id:"minecraft:gray_dye",components:{"minecraft:custom_name":'""',"minecraft:custom_data":{dcart_filler: 1b}}}}
scores.var['.airToggleTicks'] = 80
as @e[type=item,tag=dcart_new_item]:
    Data.entity('@s')['Motion'] = storage['Motion']
    Data.entity('@s')['Item'] = storage["CurrentItem"]
    tag @s remove dcart_new_item
