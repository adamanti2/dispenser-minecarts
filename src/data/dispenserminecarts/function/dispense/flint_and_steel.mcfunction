from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/flint_and_steel", "dispense/flint_and_steel")

item_tag ./flint_and_steel:
    values:
        - "flint_and_steel"

scores.var['.drop'] = 0
scores.var['.reduce'] = 0

scores.var['.success'] = 0
positioned ^ ^ ^1 if block ~ ~ ~ tnt:
    align xyz positioned ~0.5 ~0.5 ~0.5 run summon small_fireball ~ ~ ~ {Motion:[0.0d,0.001d,0.0d]}
    scores.var['.success'] = 1
unless score .success scores.var matches 1 positioned ^ ^ ^1 if block ~ ~ ~ air:
    setblock ~ ~ ~ fire
    if block ~ ~ ~ fire:
        scores.var['.success'] = 1

if score .success scores.var matches 0:
    scores.var['.click'] = 0
    scores.var['.fail'] = 1
if score .success scores.var matches 1:
    scores.var['.reduce'] = 2
