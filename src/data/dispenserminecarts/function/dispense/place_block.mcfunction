from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


# This is dependent on the dispensersplaceblocks datapack (https://gitlab.com/adamanti2/dispensers-place-blocks)
# This function replicates placeblocks:place_item

directions = {
    "east": 0,
    "south": 1,
    "north": 2,
    "west": 3,
    "up": 4,
    "down": 5,
}

unless block ~ ~ ~ #placeblocks:potentially_placeable run return 0

if entity @s[y_rotation=-135..-45] run scoreboard players set .direction dpb_var directions["east"]
if entity @s[y_rotation=-45..45] run scoreboard players set .direction dpb_var directions["south"]
if entity @s[y_rotation=45..135] run scoreboard players set .direction dpb_var directions["west"]
if entity @s[y_rotation=135..-135] run scoreboard players set .direction dpb_var directions["north"]
if entity @s[x_rotation=-90] run scoreboard players set .direction dpb_var directions["up"]
if entity @s[x_rotation=90] run scoreboard players set .direction dpb_var directions["down"]
data modify entity @e[type=armor_stand,tag=dpb_helper,limit=1] HandItems[0] set from storage dispenserminecarts:_ CurrentItem
function placeblocks:place_current

unless score .result dpb_var matches ..-1:
    if block ~ ~ ~ air:
        tag @e[type=item,tag=!dpb_checked,distance=..1] add dpb_checked
    scores.var['.drop'] = 0
