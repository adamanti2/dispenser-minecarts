from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/filled_buckets", "dispense/filled_buckets")

item_tag ./filled_buckets:
    values:
        - "#dispenserminecarts:dispense/filled_buckets/water_buckets"
        - "lava_bucket"
        - "powder_snow_bucket"

item_tag ./filled_buckets/water_buckets:
    values:
        - "water_bucket"
        - "cod_bucket"
        - "salmon_bucket"
        - "tropical_fish_bucket"
        - "pufferfish_bucket"
        - "axolotl_bucket"
        - "tadpole_bucket"

BUCKETABLE_ENTITIES = ("cod", "salmon", "tropical_fish", "pufferfish", "axolotl", "tadpole")

entityBucketsValues = []
for entityType in BUCKETABLE_ENTITIES:
    entityBucketsValues.append(f"{entityType}_bucket")
item_tag ./filled_buckets/entity_buckets {
    "values": entityBucketsValues
}

entity_type_tag ./filled_buckets/bucketable_entities {
    "values": BUCKETABLE_ENTITIES
}


scores.var['.drop'] = 0
scores.var['.reduce'] = 0

positioned ^ ^ ^1 unless block ~ ~ ~ air:
    scores.var['.click'] = 0
    scores.var['.fail'] = 1

positioned ^ ^ ^1 if block ~ ~ ~ air:
    scores.var['.reduce'] = 6
    if predicate heldTagPredicate("dispense/filled_buckets/water_buckets") run setblock ~ ~ ~ water
    if predicate heldPredicate("lava_bucket") run setblock ~ ~ ~ lava
    if predicate heldPredicate("powder_snow_bucket") run setblock ~ ~ ~ powder_snow

    if predicate heldTagPredicate("dispense/filled_buckets/entity_buckets") align xyz positioned ~0.5 ~ ~0.5:
        for entityType in BUCKETABLE_ENTITIES:
            if predicate heldPredicate(f"{entityType}_bucket"):
                summon entityType ~ ~ ~ {Tags:[dcart_new]}
        as @e[type=#./filled_buckets/bucketable_entities,tag=dcart_new]:
            tag @s remove dcart_new
            data modify entity @s {} merge from storage dispenserminecarts:_ CurrentItem.components.minecraft:bucket_entity_data
            data modify entity @s CustomName set from storage dispenserminecarts:_ CurrentItem.components.minecraft:custom_name

    if predicate ../in_nether if block ~ ~ ~ water:
        setblock ~ ~ ~ air
        playsound minecraft:block.fire.extinguish block @a
        align xyz run particle minecraft:large_smoke ~0.5 ~0.5 ~0.5 0.25 0.25 0.25 0.01 8
