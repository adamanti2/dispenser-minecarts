from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, randomizedMotion

predicate ./wind_charge {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "minecraft:wind_charge"
            }
        }
    }
}


scores.var['.drop'] = 0
scores.var['.click'] = 0
playsound minecraft:entity.wind_charge.throw neutral @a ~ ~ ~ 1 0.5

randomizedMotion(1, -4, 4, -4, 4)

scores.var['.airToggleTicks'] = 80
summon wind_charge ^ ^ ^0.5 {Tags:[dcart_new,dcart_air_toggle],acceleration_power:0.0d}
as @e[type=wind_charge,tag=dcart_new]:
    tag @s remove dcart_new
    Data.entity('@s')['Motion'] = storage['Motion']
