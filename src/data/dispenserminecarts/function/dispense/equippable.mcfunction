from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate

predicate ./equippable {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/equippable"
            }
        }
    }
}

item_tag ./equippable:
    values:
        - "#dispenserminecarts:dispense/equippable/headgear"
        - "minecraft:elytra"
        - "leather_chestplate"
        - "leather_leggings"
        - "leather_boots"
        - "chainmail_chestplate"
        - "chainmail_leggings"
        - "chainmail_boots"
        - "iron_chestplate"
        - "iron_leggings"
        - "iron_boots"
        - "golden_chestplate"
        - "golden_leggings"
        - "golden_boots"
        - "diamond_chestplate"
        - "diamond_leggings"
        - "diamond_boots"
        - "netherite_chestplate"
        - "netherite_leggings"
        - "netherite_boots"

item_tag ./equippable/headgear:
    values:
        - "#dispenserminecarts:dispense/equippable/heads"
        - "carved_pumpkin"
        - "turtle_helmet"
        - "leather_helmet"
        - "chainmail_helmet"
        - "iron_helmet"
        - "golden_helmet"
        - "diamond_helmet"
        - "netherite_helmet"

predicate ./equippable/headgear {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/equippable/headgear"
            }
        }
    }
}

item_tag ./equippable/heads:
    values:
        - "player_head"
        - "zombie_head"
        - "creeper_head"
        - "piglin_head"
        - "dragon_head"
        - "skeleton_skull"
        - "wither_skeleton_skull"

predicate ./equippable/head {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/equippable/heads"
            }
        }
    }
}

item_tag ./equippable/placeables:
    values:
        - "carved_pumpkin"
        - "wither_skeleton_skull"

predicate ./equippable/placeable {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/equippable/placeables"
            }
        }
    }
}


entity_type_tag ./always_receive_armor:
    values:
        - "player"
        - "armor_stand"

entity_type_tag ./may_receive_armor:
    values:
        - "#dispenserminecarts:dispense/always_receive_armor"
        - "skeleton"
        - "wither_skeleton"
        - "stray"
        - "zombie"
        - "husk"
        - "pillager"
        - "drowned"
        - "vindicator"
        - "villager"
        - "piglin"
        - "zombified_piglin"

for slot in ('head', 'chest', 'legs', 'feet'):
    equipment = {}
    equipment[slot] = {
        "items": "minecraft:air"
    }
    predicate f'dispenserminecarts:dispense/equippable/free_{slot}' {
        "condition": "minecraft:entity_properties",
        "entity": "this",
        "predicate": {
            "equipment": equipment
        }
    }


# First check if the headgear item can be placed onto a golem/wither structure
scores.var['.placed'] = 0
if predicate ./equippable/placeable positioned ^ ^ ^1:

    def checkAdjacentBlock(block):
        unless score .placed scores.var matches 1 if block ~ ~1 ~ block:
            scores.var['.placed'] = 1
        unless score .placed scores.var matches 1 if block ~ ~-1 ~ block:
            scores.var['.placed'] = 1
        unless score .placed scores.var matches 1 if block ~1 ~ ~ block:
            scores.var['.placed'] = 1
        unless score .placed scores.var matches 1 if block ~-1 ~ ~ block:
            scores.var['.placed'] = 1
        unless score .placed scores.var matches 1 if block ~ ~ ~1 block:
            scores.var['.placed'] = 1
        unless score .placed scores.var matches 1 if block ~ ~ ~-1 block:
            scores.var['.placed'] = 1
    
    if predicate heldPredicate("carved_pumpkin"):
        checkAdjacentBlock("snow_block")
        checkAdjacentBlock("iron_block")
        if score .placed scores.var matches 1:
            scores.var['.drop'] = 0
            setblock ~ ~ ~ carved_pumpkin
    
    unless score .placed scores.var matches 1 if predicate heldPredicate("wither_skeleton_skull"):
        checkAdjacentBlock("soul_sand")
        checkAdjacentBlock("soul_soil")
        if score .placed scores.var matches 1:
            scores.var['.drop'] = 0
            setblock ~ ~ ~ wither_skeleton_skull


# Then do actual armor equipping logic
unless score .placed scores.var matches 1:
    scores.var['.checkedMultiple'] = 0
    scores.var['.success'] = 0
    scores.var['.headgear'] = 0
    scores.var['.nodrop'] = 0
    if predicate ./equippable/headgear:
        scores.var['.headgear'] = 1
    if predicate ./equippable/head:
        scores.var['.nodrop'] = 1
    if predicate heldPredicate("carved_pumpkin"):
        scores.var['.nodrop'] = 1

    positioned ^ ^ ^1 align xyz run function ./equippable/_r_pick_entity
    function ./equippable/_r_pick_entity:
        as @e[type=#./may_receive_armor,dx=0,dy=0,dz=0,limit=1,sort=random,tag=!dcart_checked]:
            if entity @s[type=#./always_receive_armor]:
                tag @s add dcart_equipping
            unless entity @s[tag=dcart_equipping] if data entity @s {CanPickUpLoot:1b}:
                tag @s add dcart_equipping
            unless entity @s[tag=dcart_equipping]:
                scores.var['.checkedMultiple'] = 1
                tag @s add dcart_checked
                function ./equippable/_r_pick_entity
            
            if entity @s[tag=dcart_equipping]:
                tag @s remove dcart_equipping
                if score .headgear scores.var matches 1 if predicate ./equippable/free_head store success score .success scores.var run item replace entity @s armor.head from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
                unless score .success scores.var matches 1 if predicate ./equippable/free_chest store success score .success scores.var run item replace entity @s armor.chest from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
                unless score .success scores.var matches 1 if predicate ./equippable/free_legs store success score .success scores.var run item replace entity @s armor.legs from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
                unless score .success scores.var matches 1 if predicate ./equippable/free_feet store success score .success scores.var run item replace entity @s armor.feet from entity @e[type=minecraft:armor_stand,tag=dcart_helper,limit=1] weapon
                if score .success scores.var matches 1:
                    scores.var['.drop'] = 0

    if score .checkedMultiple scores.var matches 1:
        tag @e[type=#./may_receive_armor,tag=dcart_checked] remove dcart_checked
    if score .drop scores.var matches 1 if score .nodrop scores.var matches 1:
        scores.var['.drop'] = 0
        scores.var['.reduce'] = 0
        scores.var['.click'] = 0
        scores.var['.fail'] = 1
