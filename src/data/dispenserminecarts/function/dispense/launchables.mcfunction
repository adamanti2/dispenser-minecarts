from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, randomizedMotion

predicate ./launchables {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/launchables"
            }
        }
    }
}

item_tag ./launchables:
    values:
        - "arrow"
        - "spectral_arrow"
        - "tipped_arrow"
        - "snowball"
        - "egg"
        - "splash_potion"
        - "lingering_potion"
        - "experience_bottle"

entity_type_tag ./launchables/launchable_projectiles:
    values:
        - "arrow"
        - "spectral_arrow"
        - "snowball"
        - "egg"
        - "potion"
        - "experience_bottle"


scores.var['.drop'] = 0
scores.var['.click'] = 0
scores.var['.launch'] = 1


randomizedMotion(1.1, -5, 5, 0, 10)

def asProjectile():
    tag @s remove dcart_new
    Data.entity('@s')['Motion'] = storage['Motion']

scores.var['.airToggleTicks'] = 80
for genericProjectile in ("snowball", "egg", "experience_bottle"):
    if predicate heldPredicate(genericProjectile):
        summon genericProjectile ^ ^ ^0.5 {Tags:[dcart_new,dcart_air_toggle]}
        as @e[type=genericProjectile,tag=dcart_new]:
            asProjectile()

for arrowProjectile in ("arrow", "spectral_arrow"):
    if predicate heldPredicate(arrowProjectile):
        summon arrow ^ ^ ^0.5 {Tags:[dcart_new,dcart_air_toggle],pickup:1b}
        as @e[type=arrow,tag=dcart_new]:
            asProjectile()

if predicate heldPredicate("tipped_arrow"):
    summon arrow ^ ^ ^0.5 {Tags:[dcart_new,dcart_air_toggle],pickup:1b}
    as @e[type=arrow,tag=dcart_new]:
        asProjectile()
        Data.entity('@s')['item'] = storage.CurrentItem

for potionProjecitle in ("splash_potion", "lingering_potion"):
    if predicate heldPredicate(potionProjecitle):
        summon potion ^ ^ ^0.5 {Tags:[dcart_new,dcart_air_toggle]}
        as @e[type=potion,tag=dcart_new]:
            asProjectile()
            Data.entity('@s')['Item'] = storage["CurrentItem"]
