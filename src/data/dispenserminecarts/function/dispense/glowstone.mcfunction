from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


heldTagPredicate("dispense/glowstone", "dispense/glowstone")

item_tag ./glowstone:
    values:
        - "glowstone"


if block ^ ^ ^1 respawn_anchor[charges=4]:
    scores.var['.drop'] = 0
    scores.var['.reduce'] = 0
    scores.var['.click'] = 0
    scores.var['.fail'] = 1
positioned ^ ^ ^1 unless block ~ ~ ~ respawn_anchor[charges=4] if block ~ ~ ~ respawn_anchor:
    scores.var['.drop'] = 0
    playsound minecraft:block.respawn_anchor.charge block @a
    for i in reversed(range(4)):
        if block ~ ~ ~ respawn_anchor[charges=i] run setblock ~ ~ ~ respawn_anchor[charges=(i + 1)]
