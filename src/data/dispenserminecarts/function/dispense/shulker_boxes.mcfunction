from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, MAIN_SLOTS

predicate ./shulker_boxes {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/shulker_boxes"
            }
        }
    }
}

SHULKERS = ["shulker_box", "black_shulker_box", "blue_shulker_box", "brown_shulker_box", "cyan_shulker_box", "gray_shulker_box", "green_shulker_box", "light_blue_shulker_box", "light_gray_shulker_box", "lime_shulker_box", "magenta_shulker_box", "orange_shulker_box", "pink_shulker_box", "purple_shulker_box", "red_shulker_box", "white_shulker_box", "yellow_shulker_box"]

item_tag ./shulker_boxes {
    "values": SHULKERS
}

block_tag ./shulker_boxes/unplaceable:
    values:
        - "#rails"
        - "air"
        - "cave_air"
        - "void_air"


scores.var['.drop'] = 0

DIRECTIONS = {
    "up": 0,
    "down": 1,
    "east": 2,
    "south": 3,
    "west": 4,
    "north": 5,
}
positioned ^ ^ ^1:
    unless block ~ ~ ~ air:
        scores.var['.success'] = 0
        scores.var['.reduce'] = 0
        scores.var['.click'] = 0
        scores.var['.fail'] = 1
    unless block ~ ~ ~ air run return 0

    scores.var['.direction'] = -1
    unless block ~ ~-1 ~ #./shulker_boxes/unplaceable:
        scores.var['.direction'] = DIRECTIONS["up"]
    if score .direction scores.var matches -1 unless block ~ ~1 ~ #./shulker_boxes/unplaceable:
        scores.var['.direction'] = DIRECTIONS["down"]
    if score .direction scores.var matches -1 unless block ~1 ~ ~ #./shulker_boxes/unplaceable:
        scores.var['.direction'] = DIRECTIONS["west"]
    if score .direction scores.var matches -1 unless block ~-1 ~ ~ #./shulker_boxes/unplaceable:
        scores.var['.direction'] = DIRECTIONS["east"]
    if score .direction scores.var matches -1 unless block ~ ~ ~1 #./shulker_boxes/unplaceable:
        scores.var['.direction'] = DIRECTIONS["north"]
    if score .direction scores.var matches -1 unless block ~ ~ ~-1 #./shulker_boxes/unplaceable:
        scores.var['.direction'] = DIRECTIONS["south"]

    if score .direction scores.var matches -1:
        scores.var['.reduce'] = 0
        scores.var['.click'] = 0
        scores.var['.fail'] = 1
    if score .direction scores.var matches -1 run return 0

    for id in SHULKERS:
        if predicate heldPredicate(id):
            for direction, value in DIRECTIONS.items():
                if score .direction scores.var matches value:
                    setblock ~ ~ ~ f'{id}[facing={direction}]'
    Data.block('~ ~ ~').CustomName = storage.CurrentItem.components["minecraft:custom_name"]
    function ./_r_add_item:
        storage.CurrentItem.components["minecraft:container"][0].item.Slot = storage.CurrentItem.components["minecraft:container"][0].slot
        Data.block('~ ~ ~').Items.append(storage.CurrentItem.components["minecraft:container"][0].item)
        data remove storage dispenserminecarts:_ CurrentItem.components.minecraft:container[0]
        if data storage dispenserminecarts:_ CurrentItem.components.minecraft:container[0] run function ./_r_add_item
    function ./_r_add_item

