from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate


predicate ./water_bottle {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": [
                    "minecraft:potion"
                ],
                "potion": "minecraft:water"
            }
        }
    }
}

block_tag ./water_bottle/convertible_into_mud:
    values:
        - "dirt"
        - "coarse_dirt"
        - "rooted_dirt"


scores.var['.drop'] = 0
scores.var['.success'] = 0

positioned ^ ^ ^1 if block ~ ~ ~ #./water_bottle/convertible_into_mud:
    scores.var['.success'] = 1
    scores.var['.reduce'] = 4
    playsound item.bottle.empty block @a
    positioned ~ ~0.25 ~ run particle splash ^ ^ ^-0.5 0 0 0 1 10
    setblock ~ ~ ~ mud

unless score .success scores.var matches 1:
    scores.var['.reduce'] = 0
    scores.var['.click'] = 0
    scores.var['.fail'] = 1
