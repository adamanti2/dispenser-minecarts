from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate

block_tag ./droppable_containers {
  "values": [
    "minecraft:dispenser",
    "minecraft:chest",
    "minecraft:furnace",
    "minecraft:brewing_stand",
    "minecraft:trapped_chest",
    "minecraft:hopper",
    "minecraft:dropper",
    "minecraft:barrel",
    "minecraft:smoker",
    "minecraft:blast_furnace",
    "#minecraft:shulker_boxes",
    "minecraft:crafter",
    "minecraft:chiseled_bookshelf",
    "minecraft:decorated_pot",
    "minecraft:jukebox",
  ]
}

block_tag ./droppable_containers_special {
  "values": [
    "minecraft:decorated_pot",
    "minecraft:jukebox",
  ]
}


unless block ~ ~ ~ #./droppable_containers_special:
    storage["OrigStoredItems"] = Data.block("~ ~ ~")["Items"]
setblock 13 -64 2 yellow_shulker_box
item replace block 13 -64 2 container.0 from entity @s weapon ../set_to_1
store result score .result scores.var run loot insert ~ ~ ~ mine 13 -64 2 air[custom_data={drop_contents:1b}]
setblock 13 -64 2 bedrock
if score .result scores.var matches 1..:
    unless block ~ ~ ~ #./droppable_containers_special:
        store success score .result scores.var run data modify storage dispenserminecarts:_ OrigStoredItems set from block ~ ~ ~ Items
    if score .result scores.var matches 1..:
        scores.var['.drop'] = 0
        scores.var['.smoke'] = 0
