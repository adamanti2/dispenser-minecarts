from nbtlib import Byte, Float
from bolt_expressions import Data
from ../utils import scores, storage, heldPredicate, heldTagPredicate

predicate ./boats {
    "condition": "minecraft:entity_properties",
    "entity": "this",
    "predicate": {
        "equipment": {
            "mainhand": {
                "items": "#dispenserminecarts:dispense/boats"
            }
        }
    }
}

WOOD_TYPES = ('oak', 'spruce', 'birch', 'jungle', 'acacia', 'dark_oak', 'mangrove', 'cherry', 'bamboo')

boatsTagValues = []
chestBoatsTagValues = []
for wood in WOOD_TYPES:
    tagname = f"dispenserminecarts:dispense/boats/{wood}"
    boatname = "boat"
    if wood == "bamboo":
        boatname = "raft"
    chestBoatsTagValues.append(f"{wood}_chest_{boatname}")
    boatsTagValues.append("#" + tagname)
    item_tag tagname:
        values:
            - f"{wood}_{boatname}"
            - f"{wood}_chest_{boatname}"
    
    predicate tagname {
        "condition": "minecraft:entity_properties",
        "entity": "this",
        "predicate": {
            "equipment": {
                "mainhand": {
                    "items": f"#{tagname}"
                }
            }
        }
    }

item_tag ./boats {
    "values": boatsTagValues
}

item_tag ./chest_boats {
    "values": chestBoatsTagValues
}

entity_type_tag ./boats:
    values:
        - "boat"
        - "chest_boat"


scores.var['.drop'] = 0
scores.var['.success'] = 0

function ./boats/spawn_boat:
    if predicate heldTagPredicate("dispense/chest_boats"):
      for wood in WOOD_TYPES:
        if predicate f"dispenserminecarts:dispense/boats/{wood}" run summon chest_boat ^ ^ ^0.5 {Tags:["dcart_new"],Type:wood}
    unless predicate heldTagPredicate("dispense/chest_boats"):
      for wood in WOOD_TYPES:
        if predicate f"dispenserminecarts:dispense/boats/{wood}" run summon boat ^ ^ ^0.5 {Tags:["dcart_new"],Type:wood}
    as @e[type=#./boats,tag=dcart_new] positioned as @s:
        tag @s remove dcart_new
        tp @s ~ ~ ~ ~ ~
        scores.var['.success'] = 1

positioned ^ ^ ^1 if block ~ ~-1 ~ water run function ./boats/spawn_boat
unless score .success scores.var matches 1 positioned ^ ^ ^1 positioned ~ ~1 ~ if block ~ ~-1 ~ water run function ./boats/spawn_boat
unless score .success scores.var matches 1:
    scores.var['.reduce'] = 0
    scores.var['.click'] = 0
    scores.var['.fail'] = 1

