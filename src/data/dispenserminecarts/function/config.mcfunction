from ./utils import scores, storage

tellraw @s {"color":"gold","text":"Available config functions:\n  - function dispenserminecarts:config/toggle_filler_gray_dyes\n  - function dispenserminecarts:config/toggle_block_placing"}
playsound minecraft:ui.button.click player @s ~ ~ ~ 1 1.5

def toggle(scoreSource):
    scoreSource += 1
    if score scoreSource.scoreholder scoreSource.objective matches 2:
        scoreSource = 0
    if score scoreSource.scoreholder scoreSource.objective matches 0:
        storage.Text = "off"
    if score scoreSource.scoreholder scoreSource.objective matches 1:
        storage.Text = "on"

# function ./config/toggle_item_enchantment_glint:
#     toggle(scores.config['.cartglint'])
#     playsound minecraft:ui.button.click player @s ~ ~ ~ 1 1.5
#     tellraw @s [{"color":"gold","text":"Enchantment glint is now "},{"nbt":"Text","storage":"dispenserminecarts:_","bold":true,"color":"yellow"},{"text":". Crafted dispenser & dropper minecarts will no longer have an enchantment glint"}]

function ./config/toggle_filler_gray_dyes:
    toggle(scores.config['.fillerdyes'])
    playsound minecraft:ui.button.click player @s ~ ~ ~ 1 1.5
    tellraw @s [{"color":"gold","text":"Filler items are now "},{"nbt":"Text","storage":"dispenserminecarts:_","bold":true,"color":"yellow"},{"text":". Minecarts will no longer contain filler gray dyes in inaccessible slots"}]

function ./config/toggle_block_placing:
    toggle(scores.config['.blockplacing'])
    playsound minecraft:ui.button.click player @s ~ ~ ~ 1 1.5
    tellraw @s [{"color":"gold","text":"Dispenser minecart block placing is now "},{"nbt":"Text","storage":"dispenserminecarts:_","bold":true,"color":"yellow"},{"text":". Note that block placing requires adamanti's "},{"text":"dispensers-place-blocks","underlined":true,"clickEvent":{"action":"open_url","value":"https://gitlab.com/adamanti2/dispensers-place-blocks"},"color":"yellow","hoverEvent":{"action":"show_text","contents":"Open the data pack's installation page on gitlab.com"}},{"text":" datapack to be separately installed and enabled"}]
