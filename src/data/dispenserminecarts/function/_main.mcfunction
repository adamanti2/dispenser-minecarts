from bolt_expressions import Scoreboard, Data
from adamanti_bolt:utils import vanillaTag, Remember, chancePredicate
from nbtlib import Float, Byte
from ./utils import scores, tickers, angle_in_range, angle_in_range_ie, angle_in_range_ei, minecartDirections, minecartDirectionsReversed, railAngles, railAnglesDiagonal


function vanillaTag('load', ./load):
    team add dcart
    forceload add 0 0

    schedule function ./spawn_helpers 10t replace
    # setblock 13 -64 2 yellow_shulker_box

    scores.init()
    tickers.init()

    defaultConfigs = {
        # ".cartglint": 1,
        ".blockplacing": 1,
        ".fillerdyes": 1,
    }
    for config, defaultValue in defaultConfigs.items():
        if defaultValue == 0:
            scores.config[config] += 0
        else:
            store success score .cfgset scores.var run scoreboard players get config dcart.config
            if score .cfgset scores.var matches 0:
                scores.config[config] = defaultValue

function ./spawn_helpers:
    schedule function ./spawn_helpers 7s replace
    store result score .count scores.var if entity @e[type=armor_stand,tag=dcart_helper]
    if score .count scores.var matches 2.. run kill @e[type=armor_stand,tag=dcart_helper]
    unless score .count scores.var matches 1 run summon armor_stand 0 -64 0 {Small:1b,Marker:1b,Invisible:1b,Invulnerable:1b,NoGravity:1b,ShowArms:1b,Tags:[dcart_helper],DisabledSlots:4144959}

    store result score .count scores.var if entity @e[type=marker,tag=dcart_helper]
    if score .count scores.var matches 2.. run kill @e[type=marker,tag=dcart_helper]
    unless score .count scores.var matches 1 run summon marker 0.0 0.0 0.0 {Tags:[dcart_helper]}


function vanillaTag('tick', ./tick):
    # setblock 13 -63 2 bedrock
    # setblock 14 -64 2 bedrock
    # setblock 13 -64 3 bedrock
    # setblock 12 -64 2 bedrock
    # setblock 13 -64 1 bedrock

    tickers.step()
    if score 20 tickers matches 0:
        as @a[scores={scores.used_furnace_minecart=1..}]:
            scoreboard players reset @s scores.used_furnace_minecart
        
        if score .fillerdyes scores.config matches 1:
            as @e[type=hopper_minecart,limit=1] at @s run function ./_r_check_hopper_minecart
            function ./_r_check_hopper_minecart:
                tag @s add dcart_checked
                as @e[type=chest_minecart,tag=dcart,distance=..10] run tag @s add dcart_near_hopper_minecart
                as @e[type=hopper_minecart,tag=!dcart_checked,distance=..10] run tag @s add dcart_checked
                as @e[type=hopper_minecart,tag=!dcart_checked,limit=1] at @s run function ./_r_check_hopper_minecart
                tag @s remove dcart_checked

        scores.var['.first'] = 1
        as @e[type=chest_minecart,tag=dcart,tag=dcart_interacting] at @s:
            if score .first scores.var matches 1:
                scores.var['.first'] = 0
                as @a[tag=interacting_dcart] unless score @s scores.playerR1 matches -1000:
                    store result score .r0 scores.var run data get entity @s Rotation[0] 10
                    store result score .r1 scores.var run data get entity @s Rotation[1] 10
                    unless score .r0 scores.var = @s scores.playerR0 run tag @s remove interacting_dcart
                    unless score .r1 scores.var = @s scores.playerR1 run tag @s remove interacting_dcart
            function ./refresh_dcart_filler
            unless entity @a[tag=interacting_dcart,distance=..11]:
                tag @s remove dcart_interacting

    function ./tick_dcart

    if score .airToggleTicks scores.var matches 1..:
        scores.var['.airToggleTicks'] -= 1
        scores.var['.airToggleValue'] += 1
        if score .airToggleValue scores.var matches 2:
            scores.var['.airToggleValue'] = 0
        
        for i in range(2):
            if score .airToggleValue scores.var matches i as @e[type=#./air_toggleable,tag=dcart_air_toggle]:
                Data.entity('@s')['Air'] = Byte(i)
    
    entity_type_tag ./air_toggleable:
        values:
            - "item"
            - "arrow"
            - "spectral_arrow"
            - "snowball"
            - "egg"
            - "potion"
            - "experience_bottle"
            - "wind_charge"


    as @e[type=item,tag=!dcart_checked]:
        tag @s add dcart_checked
        for i in ("dispenser", "dropper"):
            name = '{"fallback":"","italic":false,"translate":"entity.dispenserminecarts.' + i + '_minecart"}'
            if data entity @s {Item:{components:{"minecraft:custom_name":name}}} at @s:
                kill @s
                loottable = f'dispenserminecarts:items/{i}_minecart'
                loot spawn ~ ~ ~ loot loottable
        if data entity @s {Item:{components:{"minecraft:custom_data":{dcart_filler:1b}}}} run kill @s
    
    as @e[type=furnace_minecart,tag=!dcart_checked]:
        tag @s add dcart_checked
        for i in ("dispenser", "dropper"):

            displayStateName = f"minecraft:{i}"
            tags = ["dcart","dcart_new","dcart_new_diagonal"]
            if i == "dropper":
                tags.append("dcart_dropper")
            items = [{Slot: Byte(3), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(4), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(5), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(6), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(7), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(8), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(12), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(13), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(14), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(15), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(16), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(17), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(21), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(22), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(23), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(24), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(25), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}, {Slot: Byte(26), id: "minecraft:gray_dye", components: {"minecraft:custom_name": '""', "minecraft:custom_data": {dcart_filler: Byte(1)}}, count: Byte(1)}]
            # customName = '{"italic":false,"translate":"entity.dispenserminecarts.' + i + '_minecart","fallback":"Minecart with ' + i.capitalize() + '"}'
            customName = '{"italic":false,"translate":"entity.dispenserminecarts.' + i + '_minecart","fallback":""}'
            
            if entity @s[name=f"Minecart with {i.capitalize()}"] at @s:

                scores.var['.angle'] = -1
                for dir, angle in railAngles.items():
                    anglef = Float(angle)
                    if block ~ ~ ~ #minecraft:rails[shape=dir]:
                        if dir in railAnglesDiagonal.keys():
                            summon chest_minecart ~ ~ ~ {CustomName:customName,Tags:(tags + ["dcart_new_diagonal"]),CustomDisplayTile:1b,DisplayOffset:8,DisplayState:{Name:displayStateName,Properties:{facing:"west"}},Rotation:[anglef,0f],Items:items}
                        else:
                            summon chest_minecart ~ ~ ~ {CustomName:customName,Tags:tags,CustomDisplayTile:1b,DisplayOffset:8,DisplayState:{Name:displayStateName,Properties:{facing:"west"}},Rotation:[anglef,0f],Items:items}
                        scores.var['.angle'] = angle
                
                if score .angle scores.var matches -1:
                    summon chest_minecart ~ ~ ~ {CustomName:customName,Tags:tags,CustomDisplayTile:1b,DisplayOffset:8,DisplayState:{Name:displayStateName,Properties:{facing:"west"}},Items:items}
                    scores.var['.angle'] = 0
                
                railAngles['default'] = 0
                scores.var['.dir'] = -1
                directions = {"up": 0, "front": 1, "left": 2, "back": 3, "right": 4}
                for angle in set(railAngles.values()):
                    if score .angle scores.var matches angle:

                        as @a[scores={scores.used_furnace_minecart=1..},limit=1,sort=nearest] if entity @s[distance=..15]:
                            scoreboard players reset @s scores.used_furnace_minecart
                            if entity @s[x_rotation=55..90]:
                                scores.var['.dir'] = directions["up"]
                            if score .dir scores.var matches -1:
                                for i, dir in enumerate(('back', 'left', 'front', 'right')):
                                    low = angle - 45 + 90 * i
                                    high = angle + 45 + 90 * i
                                    range_ = f"{low}..{high}"
                                    if entity @s[y_rotation=range_]:
                                        scores.var['.dir'] = directions[dir]
                        
                        def directionalTags(frontAngle):
                            if angle_in_range_ie(angle, frontAngle + 0, frontAngle + 90):
                                scores.var['.dir'] = directions["front"]
                            elif angle_in_range_ie(angle, frontAngle + 90, frontAngle + 180):
                                scores.var['.dir'] = directions["left"]
                            elif angle_in_range_ie(angle, frontAngle + 180, frontAngle + 270):
                                scores.var['.dir'] = directions["back"]
                            elif angle_in_range_ie(angle, frontAngle + 270, frontAngle + 360):
                                scores.var['.dir'] = directions["right"]
                        
                        if score .dir scores.var matches -1 if block ~1 ~ ~ dispenser[facing=west]:
                            directionalTags(90)
                        if score .dir scores.var matches -1 if block ~ ~ ~1 dispenser[facing=north]:
                            directionalTags(180)
                        if score .dir scores.var matches -1 if block ~-1 ~ ~ dispenser[facing=east]:
                            directionalTags(270)
                        if score .dir scores.var matches -1 if block ~ ~ ~-1 dispenser[facing=south]:
                            directionalTags(0)
                        if score .dir scores.var matches -1 if block ~ ~-1 ~ dispenser[facing=up]:
                            scores.var['.dir'] = directions["up"]
                        if score .dir scores.var matches -1 if block ~ ~1 ~ dispenser[facing=down]:
                            scores.var['.dir'] = directions["up"]
                        
                        if score .dir scores.var matches -1:
                            scores.var['.dir'] = directions["front"]
                        
                        as @e[type=chest_minecart,tag=dcart_new,limit=1] at @s:
                            tag @s remove dcart_new
                            team join dcart
                            
                            if entity @e[type=hopper_minecart,distance=..10] run tag @s add dcart_near_hopper_minecart
                            if predicate ./near_hopper run tag @s add dcart_over_hopper

                            for dir, val in directions.items():
                                minecartdir = minecartDirectionsReversed[dir]
                                # if angle_in_range_ei(angle, 90, 180) or angle_in_range_ei(angle, 225, 315):
                                if angle in (135, 315):
                                    minecartdir = minecartDirections[dir]
                                tag1 = f"dcart_{dir}"
                                tag2 = f"dcart_facing_{minecartdir}"
                                if score .dir scores.var matches val:
                                    tag @s add tag1
                                    tag @s add tag2
                                    data modify entity @s DisplayState.Properties.facing set value minecartdir
                        
                        
                
                tp @s ~ ~-400 ~
                kill @s
