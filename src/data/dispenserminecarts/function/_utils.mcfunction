from bolt_expressions import Scoreboard, Data
from adamanti_bolt:utils import Remember, chancePredicate
from nbtlib import Float, Byte
from ./utils import scores, tickers


function ./prng:
    scores.var['.rngSeed'] += 1
    if predicate chancePredicate(0.5):
        scores.var['.rngSeed'] += 1
    store result score .prng scores.var run time query gametime
    scores.var['.prng'] *= scores.var['.rngSeed']
    scores.var['.prng'] *= 314159
    scores.var['.prng'] *= 2718281
    scores.var['.prng'] *= scores.var['.prng']
    scores.var['.prng'] /= 1000
    scores.var['.prng'] *= 314159
    scores.var['.prng'] *= 2718281


for entityType in ("item",):
    fname = f"dispenserminecarts:reset_{entityType}_air"
    function fname:
        as @e[type=entityType,tag=dcart_reset_air]:
            tag @s remove dcart_reset_air
            Data.entity('@s')['Air'] = Byte(0)

predicate ./in_nether {
    "condition": "minecraft:location_check",
    "predicate": {
        "dimension": "minecraft:the_nether"
    }
}
