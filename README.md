# Dispenser Minecarts

A data pack for 1.21 that adds minecarts with dispensers and minecarts with droppers.
## Features
- Includes all* vanilla dispenser and dropper functionality.
- Dispensers & droppers can be rotated in all five directions.
- Optimized for performance on servers, such that having hundreds of minecarts loaded should have low tps impact.
- Works with Adamanti's [Dispensers Place Blocks](https://modrinth.com/datapack/dispensers-place-blocks) data pack - when both are installed, dispenser minecarts can place blocks as they move.

## How to use
- Craft them like you would expect - combine a minecart with a dispenser or dropper.
- Place items into the leftmost 3x3 grid.
- Power an activator rail to trigger the minecart.

## Limitations
- *Dispenser minecarts cannot dispense bone meal or spawn eggs.
- Minecart rotation may appear visually glitchy when crossing diagonal rails, however this has no effect on their functionality.
- Minecarts frequently colliding with other minecarts can cause them to flip their rotation server-side.
  - This is not just a visual glitch, however this is a vanilla bug that is difficult to fix with a data pack.

## Technical details
 - Checks every item and furnace minecart for the first tick of their existence. (insignificant passive performance impact)
 - Every tick, checks a few blocks at each dispenser/dropper minecart to check for powered rails and nearby hoppers. (low performance impact, depends on the number of minecarts loaded)
 - Performance-intensive functions such as scanning minecarts' inventory is optimized to only happen when necessary, e.g. when a player opens the minecart, when a hopper is nearby, or when the minecart is powered.
 - Extracting or using the gray dyes used in the minecarts' UI shouldn't be possible. However, these can be removed with the `/function dispenserminecarts:config/toggle_filler_gray_dyes` config if necessary.
 - Forceloads chunk 0 0.
 - This data pack was developed using [beet + bolt](https://github.com/mcbeet/bolt).

